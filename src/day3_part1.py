"""AoC 2021 day 3part 1"""

def get_file():
    data = open('input_day3', 'r', encoding='utf-8').read().splitlines()
    return data

def get_decimal():
    data = open('input_day3', 'r', encoding='utf-8').read().splitlines()
    gamma = 0
    epsilon = 0
    N = len(data[0])
    for n in range(N):
        count0 = sum(1 for line in data if line[n] == '0')
        count1 = len(data) - count0
        gamma *= 2
        epsilon *= 2
        if count0 < count1:
            gamma += 1
        else:
            epsilon += 1
    return(gamma, epsilon)

def get_result():
    data = open('input_day3', 'r', encoding='utf-8').read().splitlines()
    gamma = 0
    epsilon = 0
    N = len(data[0])
    for n in range(N):
        count0 = sum(1 for line in data if line[n] == '0')
        count1 = len(data) - count0
        gamma *= 2
        epsilon *= 2
        if count0 < count1:
            gamma += 1
        else:
            epsilon += 1
    answer = gamma * epsilon
    return answer

"""AoC 2021 day 3part 2"""
