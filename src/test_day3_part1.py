"""Day 3 Part 1 feature tests."""
from .day3_part1 import *
from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('features/day3_part1.feature', 'Convert two binary numbers to decimal numbers and multiplying them to get the answer of part 1.')
def test_convert_two_binary_numbers_to_decimal_numbers_and_multiplying_them_to_get_the_answer_of_part_1():
    """Convert two binary numbers to decimal numbers and multiplying them to get the answer of part 1.."""
    pass


@given('There is a binary file', target_fixture="binary_file")
def there_is_a_binary_file():
    """There is a binary file."""
    binary_file = get_file()
   


@when('Find the most common and least common binary numbers')
def find_the_most_common_and_least_common_binary_numbers(binary_file):
    """Find the most common and least common binary numbers."""
    pass


@when('convert them to decimal numbers', target_fixture="decimal_nums")
def convert_them_to_decimal_numbers():
    """convert them to decimal numbers."""
    decimal_nums = get_decimal()


@then('I multiplying those numbers together')
def i_multiplying_those_numbers_together(decimal_nums):
    """I multiplying those numbers together."""
    return decimal_nums


@then('I will get the answer of part 1')
def i_will_get_the_answer_of_part_1():
    """I will get the answer of part 1."""
    assert get_result() ==  3374136

