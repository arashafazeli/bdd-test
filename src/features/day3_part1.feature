Feature: Day 3 Part 1
    A power consumption of the submarine.
    You need to use the binary numbers in the diagnostic report to 
    generate two new binary numbers (called the gamma rate and the epsilon rate). 
    The power consumption can then be found by multiplying the gamma rate by the epsilon rate.

    Scenario Outline: Convert two binary numbers to decimal numbers and multiplying them to get the answer of part 1.
        Given There is a binary file
        When Find the most common and least common binary numbers
        And convert them to decimal numbers 
        Then I multiplying those numbers together
        Then I will get the answer of part 1
        
        